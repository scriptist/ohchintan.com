<!doctype html>
<html>
<head>

	<title>Oh Chintan...</title>
	<meta charset='utf-8' />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta property="og:title" content="Oh Chintan..."/>
	<meta property="og:description" content="Chintan has gone 50 full days without offending anybody, so it's time for this site to come to an end."/>
	<meta property="og:url" content="http://ohchintan.com/"/>
	<meta property="og:image" content="http://ohchintan.com/media/images/og-image.png"/>

	<!-- CSS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" media="all" href="/media/css/style.css" />
	<link rel="stylesheet" media="all" href="/media/css/fireworks.css" />
</head>
<body>
	<header id="header">
		<h1>Oh Chintan...</h1>
	</header>
	<div id="content">
		<div class="count">
			<div class="fireworks"></div>
			<div class="days">
				50
			</div>
			<div class="text">
				days since Chintan last offended&nbsp;everybody
			</div>
		</div>

		<div class="congratulations">
			<p>Chintan has gone 50 full days without offending anybody, so it's time for this site to come to an end.</p>
			<p>Below is an archived copy of all of Chintan's greatest moments.</p>
		</div>

		<div class="events">
			<?php
				require_once "spyc.php";
				$archive = array_reverse(Spyc::YAMLLoad('archive.yaml'));
				foreach($archive as $event) {
					?>
						<div class="event">
							<span>
								On
								<time>
									<?php

										$format = 'F jS';
										if (!is_long($event['time']))
											echo date($format);
										else
											echo date($format, $time);

									?>
								</time>
							</span>
							<?php echo $event['text']; ?>
						</div>
					<?php
				}
			?>
		</div>
	</div>
	<footer id="footer">
		&copy; 2015 <a href="http://scripti.st" target="_blank">Scriptist</a>
	</footer>

	<!-- Fireworks -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="/media/js/fireworks.js"></script>

	<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-51516140-1', 'ohchintan.com');
		ga('send', 'pageview');
	</script>
</body>
</html>
